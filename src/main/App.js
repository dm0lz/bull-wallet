import React, { Component } from "react";
import { Provider } from "react-redux";
import store from "./store";
import "./../App.css";
import HomeContainer from "../scenes/home/containers/homeContainer.js";

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <HomeContainer />
      </Provider>
    );
  }
}

export default App;
