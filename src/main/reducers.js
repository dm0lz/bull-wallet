import { combineReducers } from "redux";
import HomeReducer from "../scenes/home/reducer";

const allReducers = combineReducers({
  HomeReducer
});

export default allReducers;
