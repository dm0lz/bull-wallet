const settingCurrency = currency => {
  return {
    type: "SET_CURRENCY",
    payload: currency
  };
};

const AddingCurrency = currency => {
  return {
    type: "ADD_CURRENCY",
    payload: currency
  };
};

const processingDeposit = amount => {
  return {
    type: "PROCESS_DEPOSIT",
    payload: amount
  };
};

const processingWithdrawal = amount => {
  return {
    type: "PROCESS_WITHDRAWAL",
    payload: amount
  };
};

const raisingDepositError = error => {
  return {
    type: "SET_DEPOSIT_ERROR",
    payload: error
  };
};

const raisingWithdrawalError = error => {
  return {
    type: "SET_WITHDRAWAL_ERROR",
    payload: error
  };
};

const loggingTransaction = transaction => {
  return {
    type: "LOG_TRANSACTION",
    payload: transaction
  };
};

export const setCurrency = currency => {
  return dispatch => {
    dispatch(settingCurrency(currency.value));
  };
};

export const addCurrency = currency => {
  return dispatch => {
    const upCasedCurrency = currency.toUpperCase();
    dispatch(AddingCurrency(upCasedCurrency));
  };
};

export const deposit = amount => {
  return dispatch => {
    if (amount >= 10000) {
      return dispatch(
        raisingDepositError({
          message: "To comply with AML rules you cannot deposit more than 10000"
        })
      );
    }
    dispatch(processingDeposit(amount));
    dispatch(logTransactionn(amount, "deposit"));
  };
};

export const withdraw = (amount, currentBalance) => {
  return dispatch => {
    if (amount >= currentBalance) {
      return dispatch(
        raisingWithdrawalError({
          message: " Insufficient funds"
        })
      );
    }
    dispatch(processingWithdrawal(amount));
    dispatch(logTransactionn(amount, "withdrawal"));
  };
};

const logTransactionn = (amount, type) => {
  return dispatch => {
    const payload = {
      createdAt: Math.round(new Date().getTime() / 1000),
      type,
      amount
    };
    dispatch(loggingTransaction(payload));
  };
};
