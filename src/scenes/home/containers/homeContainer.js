import React, { Component } from "react";
import * as home_actions from "./../actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Select from "react-select";
import TransactionHistory from "../components/transactionHistoryComponent";
import InputFormComponent from "../components/inputFormComponent";
import UserBalanceComponent from "../components/userBalanceComponent";
import Websocket from "react-websocket";

class HomeContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newCurrency: "",
      depositAmount: 0,
      withdrawalAmount: 0,
      selectedCurrencyOption: this.props.selectedCurrency
    };
  }

  handleChange = selectedCurrencyOption => {
    this.props.setCurrency(selectedCurrencyOption);
  };

  handleNewCurrencySubmit = currency => {
    currency.preventDefault();
    this.props.addCurrency(this.state.newCurrency);
    this.setState({ newCurrency: "" });
  };

  handleWithdrawalSubmit = event => {
    event.preventDefault();
    const amount = this.state.withdrawalAmount;
    this.props.withdraw(parseFloat(amount), this.props.balance);
    this.setState({ withdrawalAmount: 0 });
  };

  handleDepositSubmit = event => {
    event.preventDefault();
    const amount = this.state.depositAmount;
    this.props.deposit(parseFloat(amount));
    this.setState({ depositAmount: 0 });
  };

  handleNewCurrencyChange = event => {
    this.setState({
      newCurrency: event.target.value
    });
  };

  handleDepositChange = event => {
    this.setState({
      depositAmount: event.target.value
    });
  };

  handleWithdrawalChange = event => {
    this.setState({
      withdrawalAmount: event.target.value
    });
  };

  handleWsOpen = event => {
    // console.log(event);
    this.sendMessage();
  };

  handleWsData = event => {
    // Unfortunately, i could not find a fiat exchange rate api that provides websocket stream for free.
    const msg = JSON.parse(event);
    console.log(msg);
    // this.props.updateFiatRate(fiat, rate);
  };

  sendMessage = () => {
    const message = {
      type: "subscribe",
      channels: [
        {
          name: "ticker",
          product_ids: ["ETH-EUR", "ETH-USD", "BTC-USD"]
        }
      ]
    };
    this.refWebSocket.sendMessage(JSON.stringify(message));
  };

  render() {
    const selectedCurrencyOption = this.props.selectedCurrency;
    const newCurrency = this.props.newCurrency || this.state.newCurrency;
    const availableCurrencies = this.props.currencies;
    const eurBalance = parseFloat(this.props.balance * this.props.eurRate);
    const chfBalance = parseFloat(this.props.balance * this.props.chfRate);
    return (
      <section>
        <UserBalanceComponent
          selectedCurrencyOption={selectedCurrencyOption}
          balance={this.props.balance}
          eurBalance={eurBalance}
          chfBalance={chfBalance}
          currencies={this.props.currencies}
        />
        <hr />
        <Select
          value={selectedCurrencyOption}
          onChange={this.handleChange}
          options={availableCurrencies}
        />
        <hr />
        <InputFormComponent
          handleSubmit={this.handleNewCurrencySubmit}
          title="Currency Name"
          formType="text"
          value={newCurrency}
          onInputChange={this.handleNewCurrencyChange}
          error=""
        />
        <hr />
        <InputFormComponent
          handleSubmit={this.handleDepositSubmit}
          title="Deposit Amount"
          formType="number"
          value={this.state.depositAmount}
          onInputChange={this.handleDepositChange}
          error={this.props.depositError.message}
        />
        <hr />
        <InputFormComponent
          handleSubmit={this.handleWithdrawalSubmit}
          title="Withdrawal Amount"
          formType="number"
          value={this.state.withdrawalAmount}
          onInputChange={this.handleWithdrawalChange}
          error={this.props.withdrawalError.message}
        />
        <hr />
        <TransactionHistory history={this.props.transactionHistory} />

        <Websocket
          url="wss://ws-feed.pro.coinbase.com"
          onMessage={this.handleWsData}
          onOpen={this.handleWsOpen}
          ref={Websocket => {
            this.refWebSocket = Websocket;
          }}
        />
      </section>
    );
  }
}

const mapStateToProps = state => {
  return {
    selectedCurrency: state.HomeReducer.selectedCurrency,
    currencies: state.HomeReducer.currencies,
    balance: state.HomeReducer.balance,
    eurRate: state.HomeReducer.eurRate,
    chfRate: state.HomeReducer.chfRate,
    depositError: state.HomeReducer.depositError,
    withdrawalError: state.HomeReducer.withdrawalError,
    transactionHistory: state.HomeReducer.transactionHistory
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(home_actions, dispatch);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeContainer);
