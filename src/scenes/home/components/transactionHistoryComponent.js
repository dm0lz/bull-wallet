import React from "react";

const TransactionHistory = transaction => {
  return (
    <ul>
      {transaction.history.map(event => (
        <li>
          Amount : {event.amount} - Type: {event.type} - Timestamps:{" "}
          {event.createdAt}
        </li>
      ))}
    </ul>
  );
};

export default TransactionHistory;
