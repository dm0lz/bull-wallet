import React from "react";

const UserBalanceComponent = ({
  selectedCurrencyOption,
  balance,
  eurBalance,
  chfBalance,
  currencies
}) => {
  return (
    <section>
      <h1>Selected currency : {selectedCurrencyOption} </h1>
      <h1>current USD balance : {balance} </h1>
      <h1>current EUR balance : {eurBalance}</h1>
      <h1>current CHF balance : {chfBalance}</h1>
      <hr />
      <h1>
        currencies list :{currencies.map(currency => currency.value).join(", ")}
      </h1>
    </section>
  );
};

export default UserBalanceComponent;
