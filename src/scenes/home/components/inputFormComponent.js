import React from "react";
import TransactionError from "./errorComponent";

const InputFormComponent = ({
  handleSubmit,
  title,
  formType,
  value,
  onInputChange,
  error
}) => {
  return (
    <section>
      <form onSubmit={handleSubmit}>
        <label>
          {title} :
          <input type={formType} value={value} onChange={onInputChange} />
        </label>
        <input type="submit" value="Submit" />
      </form>
      <TransactionError message={error} />
    </section>
  );
};

export default InputFormComponent;
