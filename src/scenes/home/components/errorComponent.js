import React from "react";

const ErrorComponent = error => {
  if (error.message.length > 0) {
    return <p style={{ color: "red" }}>{error.message}</p>;
  }
  return null;
};

export default ErrorComponent;
