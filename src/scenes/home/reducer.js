const initialState = {
  selectedCurrency: "USD",
  currencies: [
    { value: "USD", label: "USD" },
    { value: "EUR", label: "EUR" },
    { value: "CHF", label: "CHF" }
  ],
  newCurrency: "",
  balance: 0,
  eurRate: 0.88,
  chfRate: 0.99,
  depositError: { message: "" },
  withdrawalError: { message: "" },
  transactionHistory: []
};

export default function homeReducer(state = initialState, action = {}) {
  switch (action.type) {
    case "SET_CURRENCY":
      return {
        ...state,
        selectedCurrency: action.payload
      };
    case "ADD_CURRENCY":
      return {
        ...state,
        currencies: [
          ...state.currencies,
          { value: action.payload, label: action.payload }
        ],
        newCurrency: ""
      };
    case "PROCESS_DEPOSIT":
      return {
        ...state,
        balance: state.balance + parseFloat(action.payload),
        depositError: { message: "" }
      };
    case "PROCESS_WITHDRAWAL":
      return {
        ...state,
        balance: state.balance - parseFloat(action.payload),
        withdrawalError: { message: "" }
      };
    case "TRANSFER_CURRENCY":
      return {
        ...state,
        usdBalance: action.payload
      };
    case "SET_DEPOSIT_ERROR":
      return {
        ...state,
        depositError: action.payload
      };
    case "SET_WITHDRAWAL_ERROR":
      return {
        ...state,
        withdrawalError: action.payload
      };
    case "LOG_TRANSACTION":
      return {
        ...state,
        transactionHistory: [...state.transactionHistory, action.payload]
      };
    default:
      return state;
  }
}
